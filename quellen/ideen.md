Auftrag:

> Einblick, was Computermuseen aus Sicht des CCC leisten sollten, um die neue Gesellschafts-, Freizeit- und Arbeitsweltentwicklung in Bezug auf die Digitalisierung zu begleiten

- Format: 15 Min Vortrag, danach Diskussionsrunde
- Termin: 19. September 2020, 10-12 Uhr
- Publikum: Förderer, befreundete Museen, Kulturinteressierte

--------------------------------------------------------------------------------

Leitsprüche:

- Informatikunterricht muss zeitgemäß sein. Museen dürfen zeitlos sein.
- Museen haben die Gelegenheit, besonders interdisziplinär zu denken.
- Sind Programmierer Ingenieure? Autoren? Künstler? Noch wissen wir es nicht. Diese Gelegenheit müssen wir nutzen.
- Schiller, Briefe über die ästhetische Erziehung des Menschen, 15. Brief: "Der Mensch spielt nur, wo er in voller Bedeutung des Worts Mensch ist, und er ist nur da ganz Mensch, wo er spielt."

Ansätze:

- frühe mechanische Rechner: Computer sind keine Magie, sondern man kann ihnen sogar zuschauen beim Rechnen
  - siehe auch handgebaute Rechner in Minecraft
- Game of Life, Mandelbrotmenge: emergente Komplexität und chaotisches Verhalten (kleine Änderung, große Wirkung)
  - wichtig, weil Verständnis emergenter Komplexität erlaubt, das Verhalten großer Systeme (Gesellschaft, Wirtschaft, etc.) intuitiv zu verstehen
- Deep Dreaming: Ähnlichkeit zu kreativen Prozessen im Gehirn
  - wichtig, weil schnellerer Fortschritt auch immer schneller Weltbilder über den Haufen wirft (siehe geozentrisches Weltbild, siehe Rassentheorie, siehe "Tiere empfinden Schmerz", siehe Bewusstsein in Pflanzen)
  - lieber frühzeitig lernen, auf der Seite der Wahrheit zu stehen als auf der Seite der Ideologie
- Umgang mit unvollständigen Informationen (TODO: gutes Beispiel dazu; war da was gutes in dem VSauce-Video zu "the dark")
  - Option: einfach aus meinem Berufsalltag erzählen

Quellen:

- "The Art of Code" https://www.youtube.com/watch?v=6avJHaC3C2U

--------------------------------------------------------------------------------

Moles Einwand: Museen sollten immer in irgendeiner Form "zum Anfassen" sein, nicht nur rein digital.

--------------------------------------------------------------------------------

Bonusgedanke: Woher kommen die Covididioten? Und warum hat das was mit diesem Vortrag zu tun?

- siehe Langzitat in <file:quellen/facebook-kommentar.md>
- Internet und Social Media lassen die Grenze zwischen öffentlicher und privater Sphäre verschwimmen
- siehe Unwillen vieler Leute, öffentliche Regeln zu befolgen (Maskenpflicht)
