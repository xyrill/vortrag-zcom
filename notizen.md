## Einleitung

- Auftrag: Einblick, was Computermuseen aus Sicht des CCC leisten sollten, um die neue Gesellschafts-, Freizeit- und Arbeitsweltentwicklung in Bezug auf die Digitalisierung zu begleiten

- Vortragstitel nicht missverstehen -> Schiller-Zitat
- Maschinisierung ist Ideologie der Zeit: Selbstoptimierung, Ökonomisierung des Privaten
- Menschen sollen nicht wie Maschinen werden
- und doch: Computer gaben mir Lektionen fürs Leben

## Lektion 1: Game of Life

- Überleben: 2 oder 3 Nachbarn
- Fortpflanzen: 3 Nachbarn

- "Dann hat man Stillleben gefunden."
- "Dann hat man Oszillatoren gefunden."
- "Dann hat man Raumschiffe gefunden."
- "Dann hat man Raumschiffkanonen gefunden."

- nach dem Video: Verständnis emergenter Komplexität erlaubt, das Verhalten großer Systeme intuitiv nachzuvollziehen
- siehe: Gesellschaft, Wirtschaft, Politik

## Lektion 2: Künstliche Intelligenz

- "Früher dachte man, Schach erfordert Intelligenz."
- "Okay okay, aber Go erfordert Intelligenz."
- "Okay okay, aber das ist ja nur Spielerei. Unsere Arbeitsplätze sind sicher!"

- Geschichte der Computer ist eine Geschichte gebrochener Erwartungen
- nicht nur in der Technik: geozentrisches Weltbild, Äther, Rassentheorie, binäre Geschlechter, Schmerzempfinden in Tieren, Bewusstsein in Pflanzen, etc.

- Deep Dream: Netz ist auf Erkennung von Hunden trainiert
- all das **trotz** bisher eher lachhaftem Verständnis von Neurologie

- Ideologie oder Wahrheit: nicht missverstehen als Technikgläubigkeit -> autonomes Fahren
- Überleitung: immer bereit sein, sich von der Realität belehren zu lassen

## Lektion 3: Mein Berufsalltag

- Gute IT-Arbeiter sind immer auch Wissenschaftler
- Anekdote: NFS 3 vs. 4
- Praktisches Handeln heißt: Entscheiden im Angesicht unvollständiger Information.

## Zusammenfassung

1. emergente Komplexität verstehen
2. aus neuen Informationen lernen
3. mit unvollständiger Information handeln

- Synthese: all das fehlt den Querdenkern
- Großzitat: Facebook-User "Vapers Guru"

> In Berlin haben die Leute nicht gegen Corona Regulierungen demonstriert. In Berlin haben Menschen für Ihr Recht demonstriert, von der Komplexität der Welt überfordert zu sein.
> Gegner einer Impfung, die es noch gar nicht gibt. Verhutzelte Rentnerinnen, die im Rausch der Euphorie den Tag der Freiheit ausrufen. Menschen die tatsächlich glauben, die Maskenpflicht würde dadurch sofort abgeschafft. Die Journalistin Dunja Hayali wird bepöbelt, sie hätte die Versammlung auflösen lassen. Langhaarige Metallfans tragen die Flagge eines Reichs umher, in dem sie für ihre Frisur zusammengeknüppelt worden wären und in der die Impfpflicht polizeilich durchgesetzt wurde. Thor Steinar T-Shirts und Pegida Schilder. Und irgendwo sitzt eine junge Frau in Hippie Klamotten mit einem Schild auf dem Rücken: „Deutschland braucht Jesus“.
> ...
> Sie demonstrieren für Freiheit und merken nicht einmal, dass sie dabei eine der größten Freiheiten bereits in Anspruch nehmen. Sie kommentieren auf Social Media über den Verlust von Meinungsfreiheit und bemerken den Widerspruch nicht einmal.
> Sie glauben tatsächlich die demokratische Mehrheit seien die Diktatoren, weil sie vor lauter Freiheit vergessen haben, was Unfreiheit tatsächlich bedeutet.

- Endfolie: nochmal die Lektionen
- "nicht das erwartete Ende"
- noch eine neue Sachkompetenz im Lehrplan hilft nicht, wenn wir nicht diese Sozialkompetenzen erwerben
- "Auch ich halte mich nicht immer daran. Aber manchmal habe ich lichte Momente. Davon brauchen wir mehr. Vielen Dank."
